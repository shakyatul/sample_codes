package in.forsk.helloworld;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import in.forsk.helloworld.iiitk_saurabh.Screen2;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private Button firstBtn, secondBtn, thirdBtn, forthBtn, fifthBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Write Code here
        firstBtn = (Button) findViewById(R.id.button1);
        secondBtn = (Button) findViewById(R.id.button2);
        thirdBtn = (Button) findViewById(R.id.button3);
        forthBtn = (Button) findViewById(R.id.button4);
        fifthBtn = (Button) findViewById(R.id.button5);

//        firstBtn.setOnClickListener(this);
        secondBtn.setOnClickListener(this);
        thirdBtn.setOnClickListener(this);
        forthBtn.setOnClickListener(this);
        fifthBtn.setOnClickListener(this);

        firstBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Write Code Here

                Toast.makeText(MainActivity.this, "Button One Click", Toast.LENGTH_SHORT).show();

                Log.e("Main Activity", "Button One Clicked");

                //Intent Code
                Intent myIntent;
                myIntent = new Intent(MainActivity.this, Screen2.class);
                myIntent.putExtra("KEY", "Coming from Button One");
                startActivityForResult(myIntent, 100);
            }
        });

    }

    @Override
    public void onClick(View v) {
        System.out.println("Button is Clicked");

        switch (v.getId()) {
            case R.id.button1:
                Toast.makeText(MainActivity.this, "Button One Click", Toast.LENGTH_SHORT).show();

                Log.e("Main Activity", "Button One Clicked");
                break;
            case R.id.button2:

                Intent intent1 = new Intent(Intent.ACTION_VIEW, Uri.parse("https://m.facebook.com/forsklabs/"));
                startActivity(intent1);

                break;
            case R.id.button3:
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:+919928492120"));
                startActivity(callIntent);
                break;
            case R.id.button4:
                Intent intent2 = new Intent("android.media.action.IMAGE_CAPTURE");
                startActivity(intent2);
                break;
            case R.id.button5:
//                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
//                startActivityForResult(intent, PICK_CONTACT);

                Intent send_intent = new Intent(Intent.ACTION_SEND);
                send_intent.setType("text/plain");
                send_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                send_intent.putExtra(android.content.Intent.EXTRA_TEXT, "Standing on the Moon!");

                Intent chooser = Intent.createChooser(send_intent, "Chooser Title!");

                if (chooser.resolveActivity(getPackageManager()) != null) {
                    startActivity(chooser);
                }
                break;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                String result = data.getStringExtra("result_key");
                Log.d("main activity", result);
            }


        }
    }
}
