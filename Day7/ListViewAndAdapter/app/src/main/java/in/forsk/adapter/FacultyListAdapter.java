package in.forsk.adapter;

import in.forsk.R;
import in.forsk.wrapper.FacultyWrapper;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;


// comment by Sylvester on 23 dec 2015 at 1:01 PM



public class FacultyListAdapter extends BaseAdapter {
	private final static String TAG = FacultyListAdapter.class.getSimpleName();
	Context context;
	ArrayList<FacultyWrapper> mFacultyDataList;
	LayoutInflater inflater;

	AQuery aq;

	public FacultyListAdapter(Context context, ArrayList<FacultyWrapper> mFacultyDataList) {
		this.context = context;
		this.mFacultyDataList = mFacultyDataList;

		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		aq = new AQuery(context);
	}

	@Override
	public int getCount() {
		return mFacultyDataList.size();
	}

	@Override
	public Object getItem(int position) {
		return mFacultyDataList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.row_faculty_profile_list, null);
		}

		ImageView profileIv = (ImageView) convertView.findViewById(R.id.profileIv);

		TextView nameTv = (TextView) convertView.findViewById(R.id.nameTv);
		TextView  departmentTv = (TextView) convertView.findViewById(R.id.departmentTv);
		TextView reserch_areaTv = (TextView) convertView.findViewById(R.id.reserch_areaTv);

		FacultyWrapper obj = mFacultyDataList.get(position);

		AQuery temp_aq = aq.recycle(convertView);
		temp_aq.id(profileIv).image(obj.getPhoto(), true, true, 200, 0);

		nameTv.setText(obj.getFirst_name() + " " + obj.getLast_name());
		departmentTv.setText(obj.getDepartment());
		reserch_areaTv.setText(obj.getReserch_area());

		return convertView;
	}
}
